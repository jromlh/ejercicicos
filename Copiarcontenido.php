<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">

    <title>Copiar contenido</title>
  </head>
  <body>
    <div >
      <input type="text" id="tx1" placeholder="Ingresa un comentario">
    </div>
    <br>
    <br>
    <div>
      <button id="btn" >Copiar contenido</button>
    </div>
    <br>
    <br>
    <div >
      <input type="text" id="tx2" placeholder="Esperando...">
    </div>


        <script type="text/javascript">
        var boton=document.getElementById("btn");
        let entrada=document.getElementById("tx1");
        var salida=document.getElementById("tx2");

        boton.addEventListener("click",function (){
          console.log("entre");
          let palabra=entrada.value;
          console.log(palabra);
          salida.value = palabra;
        });
        </script>
  </body>
</html>
