<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Carrito</title>
  </head>
  <body>
    <table>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>PRECIO UNITARIO</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
      </tr>

      <?php
      echo "<tr>";
      for($i=1; $i<6; $i++){
        echo "<td>".$i."</td>";
        echo "<td> Libro".$i."</td>";
        echo "<td> Buen libro ".$i."</td>";
        echo "<td> <span id='precio$i'>".($i*100)."</span> </td>";
        echo "<td>
                <button type='button' id='btnMenos$i'>-</button>
                <span id='cantidad$i'>1</span>
                <button type='button' id='btnMas$i'>+</button>
              </td>";
        echo "<td> <span id='total$i'>".($i*100)."</span> </td>";

        echo "</tr>";
      }

        ?>

    </table>

    <script type="text/javascript">
        var menos1=document.getElementById('btnMenos1');
        var menos2=document.getElementById('btnMenos2');
        var menos3=document.getElementById('btnMenos3');
        var menos4=document.getElementById('btnMenos4');
        var menos5=document.getElementById('btnMenos5');

        var mas1=document.getElementById('btnMas1');
        var mas2=document.getElementById('btnMas2');
        var mas3=document.getElementById('btnMas3');
        var mas4=document.getElementById('btnMas4');
        var mas5=document.getElementById('btnMas5');



        var contador1=1;
        var contador2=1;
        var contador3=1;
        var contador4=1;
        var contador5=1;


        mas1.addEventListener("click", function(){
          contador1++;
          var cantidad=document.getElementById('cantidad1');
          cantidad.innerHTML=contador1;
          var total=contador1*100;
          var precioTotal=document.getElementById('total1');
          precioTotal.innerHTML=total;
        });

        menos1.addEventListener("click", function(){
          contador1--;
          var cantidad=document.getElementById('cantidad1');
          cantidad.innerHTML=contador1;
          var total=contador1*100;
          var precioTotal=document.getElementById('total1');
          precioTotal.innerHTML=total;
        });




        mas2.addEventListener("click", function(){
          contador2++;
          var cantidad=document.getElementById('cantidad2');
          cantidad.innerHTML=contador2;
          var total=contador2*200;
          var precioTotal=document.getElementById('total2');
          precioTotal.innerHTML=total;
        });

        menos2.addEventListener("click", function(){
          contador2--;
          var cantidad=document.getElementById('cantidad2');
          cantidad.innerHTML=contador2;
          var total=contador2*200;
          var precioTotal=document.getElementById('total2');
          precioTotal.innerHTML=total;
        });



        mas3.addEventListener("click", function(){
          contador3++;
          var cantidad=document.getElementById('cantidad3');
          cantidad.innerHTML=contador3;
          var total=contador3*300;
          var precioTotal=document.getElementById('total3');
          precioTotal.innerHTML=total;
        });

        menos3.addEventListener("click", function(){
          contador3--;
          var cantidad=document.getElementById('cantidad3');
          cantidad.innerHTML=contador3;
          var total=contador3*300;
          var precioTotal=document.getElementById('total3');
          precioTotal.innerHTML=total;
        });



        mas4.addEventListener("click", function(){
          contador4++;
          var cantidad=document.getElementById('cantidad4');
          cantidad.innerHTML=contador4;
          var total=contador4*400;
          var precioTotal=document.getElementById('total4');
          precioTotal.innerHTML=total;
        });

        menos4.addEventListener("click", function(){
          contador4--;
          var cantidad=document.getElementById('cantidad4');
          cantidad.innerHTML=contador4;
          var total=contador4*400;
          var precioTotal=document.getElementById('total4');
          precioTotal.innerHTML=total;
        });




        mas5.addEventListener("click", function(){
          contador5++;
          var cantidad=document.getElementById('cantidad5');
          cantidad.innerHTML=contador5;
          var total=contador5*500;
          var precioTotal=document.getElementById('total5');
          precioTotal.innerHTML=total;
        });

        menos5.addEventListener("click", function(){
          contador5--;
          var cantidad=document.getElementById('cantidad5');
          cantidad.innerHTML=contador5;
          var total=contador5*500;
          var precioTotal=document.getElementById('total5');
          precioTotal.innerHTML=total;
        });








    </script>

  </body>
</html>
