<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Catalogo</title>
    <script src="https://code.jquery.com/jquery-3.3.1.js">

    </script>
  </head>
  <body>
    <table>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th></th>
      </tr>

      <?php
      echo "<tr>";
      for($i=1; $i<6; $i++){
        echo "<td>".$i."</td>";
        echo "<td> Libro".$i."</td>";
        echo "<td>  <button type='button' name='button' onclick='llamar($i)' >Agregar al carrito</button>  </td>";
        echo "</tr>";
      }
      ?>
    </table>
    <br>
    <br>
    <table id="carrito">
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>PRECIO UNITARIO</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
      </tr>
      <tr>

      </tr>

    </table>
  </body>

  <script type="text/javascript">

    function llamar(id){
      $.ajax({
        url:"server.php",
        type:"post",
        data:{identificador:id}
      }).done(function(respuesta){
        let datos=JSON.parse(respuesta);
        document.getElementById("carrito").insertRow(-1).innerHTML="<td>"+datos[0]+"</td><td>"+datos[1]+"</td><td>"+datos[2]+"</td><td>"+datos[3]+"</td><td><button type='button' class='btn' val=-1>-</button><span>1</span><button type='button' class='btn' val=1>+</button></td><td>"+datos[3]+"</td>";
        actualizarFuncion();
      });
    }

    function actualizarFuncion(){
      var botones=document.querySelectorAll('.btn');

      botones.forEach(function(btn){
        btn.addEventListener("click",function(evt){
          var boton=evt.target;
          var tr=boton.parentNode.parentNode;

          var tds= tr.querySelectorAll("td");
          console.log(tds);
          var vu=tds[3].innerHTML;
          var cantidad=parseInt(tds[4].querySelector("span").innerHTML);

          var valor=parseInt(boton.getAttribute("val"));

          cantidad+=valor;

          var total=cantidad*vu;

          tds[4].querySelector("span").innerHTML=cantidad;
          tds[5].innerHTML=total;
        })
      })
    }


  </script>
</html>
