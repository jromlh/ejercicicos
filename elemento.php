<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Elementos </title>
  </head>
  <body>
    <div id="div1">El texto superior se ha creado dinámicamente.</div>
    <script type="text/javascript">

    document.body.onload = addElement;

    function addElement () {
      // crea un nuevo div
      // y añade contenido
      var newDiv = document.createElement("button");
      var newContent = document.createTextNode("¡Hola! ¿Cómo estas el día de hoy?");
      newDiv.appendChild(newContent); //añade texto al boton.

      //agregamos las clases
      newDiv.classList.add('btn','btnA');

      //agregamos el id
      newDiv.setAttribute("id","btn1");

      //agregamos el evento
      newDiv.addEventListener("click",function(){
        alert("has precionado el boton");
      });

      // añade el elemento creado y su contenido al DOM
      var currentDiv = document.getElementById("div1");
      document.body.insertBefore(newDiv, currentDiv);

    }
    </script>
  </body>
</html>
