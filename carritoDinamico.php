<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Carrito</title>
  </head>
  <body>
    <table id="tabla">
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>PRECIO UNITARIO</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
      </tr>
      <tbody>
        <td>1</td>
        <td>Libro</td>
        <td>Buen libro</td>
        <td>100</td>
        <td>
          <button type='button' class='btn' val=-1>-</button>
          <span>1</span>
          <button type='button' class='btn' val=1>+</button>
        </td>
        <td>100</td>
      </tbody>

    </table>

    <button type="button" id="btnNuevo">Nuevo</button>

    <script type="text/javascript">
    var botonNuevo=document.getElementById('btnNuevo');
    var contadorProductos=1;



    botonNuevo.addEventListener("click", function(){
      document.getElementById("tabla").insertRow(-1).innerHTML = `
      <td>1</td>
      <td>Libro</td>
      <td>Buen libro</td>
      <td>100</td>
      <td>
        <button type='button' class='btn' val=-1>-</button>
        <span>1</span>
        <button type='button' class='btn' val=1>+</button>
      </td>
      <td>100</td>
      `;
      actualizarFuncion();
    });


    function actualizarFuncion(){
      var botones=document.querySelectorAll('.btn');

      botones.forEach(function(btn){
        btn.addEventListener("click",function(evt){
          var boton=evt.target;
          var tr=boton.parentNode.parentNode;

          var tds= tr.querySelectorAll("td");
          console.log(tds);
          var vu=tds[3].innerHTML;
          var cantidad=parseInt(tds[4].querySelector("span").innerHTML);

          var valor=parseInt(boton.getAttribute("val"));

          cantidad+=valor;

          var total=cantidad*vu;

          tds[4].querySelector("span").innerHTML=cantidad;
          tds[5].innerHTML=total;
        })
      })
    }





    </script>

  </body>
</html>
