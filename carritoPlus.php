<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Carrito</title>
  </head>
  <body>
  	<p>Hola Roman</p>
    <table>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>PRECIO UNITARIO</th>
        <th>CANTIDAD</th>
        <th>TOTAL</th>
      </tr>

      <?php
      echo "<tr>";
      for($i=1; $i<6; $i++){
        echo "<td>".$i."</td>";
        echo "<td> Libro".$i."</td>";
        echo "<td> recomendado ".$i."</td>";
        echo "<td>".($i*100)."</td>";
        echo "<td>
                <button type='button' class='btn' val=-1>-</button>
                <span>1</span>
                <button type='button' class='btn' val=1>+</button>
              </td>";
        echo "<td>".($i*100)."</td>";

        echo "</tr>";
      }

        ?>

    </table>

    <script type="text/javascript">
    var botones=document.querySelectorAll('.btn');

    botones.forEach(function(btn){
      btn.addEventListener("click",function(evt){
        var boton=evt.target;
        var tr=boton.parentNode.parentNode;

        var tds= tr.querySelectorAll("td");
        console.log(tds);
        var vu=tds[3].innerHTML;
        var cantidad=parseInt(tds[4].querySelector("span").innerHTML);

        var valor=parseInt(boton.getAttribute("val"));

        cantidad+=valor;

        var total=cantidad*vu;

        tds[4].querySelector("span").innerHTML=cantidad;
        tds[5].innerHTML=total;
      })
    });


    </script>

  </body>
</html>
